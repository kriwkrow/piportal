#!/bin/bash -e

OF_DIR="of"
OF_VERSION="v0.11.0"
OF_URL="https://openframeworks.cc/versions/${OF_VERSION}/of_${OF_VERSION}_linuxarmv6l_release.tar.gz"

PORTAL_APP_REPO="https://github.com/kr15h/portalApp.git"
ETHERDREAM_REPO="https://github.com/kr15h/ofxEtherdream.git"
ILDA_REPO="https://github.com/kr15h/ofxIlda.git"
MQTT_REPO="https://github.com/256dpi/ofxMQTT.git"
MQTT_VERSION="v1.3.0"

# Download openFrameworks
mkdir temp
cd temp
wget --tries 5 --quiet ${OF_URL}
mkdir ${OF_DIR}
tar vxfz "$(ls | grep .tar.gz | head -1)" -C ${OF_DIR} --strip-components 1
cd ../
mv "temp/${OF_DIR}" "${ROOTFS_DIR}/home/pi/"

# Download and configure addons
git clone ${ETHERDREAM_REPO}
mv ofxEtherdream ${ROOTFS_DIR}/home/pi/${OF_DIR}/addons/
git clone ${ILDA_REPO}
mv ofxIlda ${ROOTFS_DIR}/home/pi/${OF_DIR}/addons/
git clone ${MQTT_REPO}
cd ofxMQTT && git checkout ${MQTT_VERSION} && cd ../
mv ofxMQTT ${ROOTFS_DIR}/home/pi/${OF_DIR}/addons/

# Download and configure portal app
echo "Current working dir: $(pwd)"
mv /builds/kriwkrow/piportal/portalApp ${ROOTFS_DIR}/home/pi/${OF_DIR}/apps/myApps/

# Enter chroot on Raspberry Pi (act as if you were root on Pi)
on_chroot << EOF

# Change owner to pi
chown --recursive pi:pi "/home/pi/${OF_DIR}"

# Install openFrameworks dependencies
cd /home/pi/${OF_DIR}/scripts/linux/debian
sed -i "s/apt-get install/apt install -y/g" install_dependencies.sh
echo "Debug install_dependencies.sh"
cat install_dependencies.sh | grep apt
./install_dependencies.sh

# Apply legacy datatype fix and compile openframeworks
cd /home/pi/${OF_DIR}/libs/openFrameworks/utils
sed -i "s/OF_USE_LEGACY_VECTOR_MATH 0/OF_USE_LEGACY_VECTOR_MATH 1/g" ofConstants.h
echo "Debug lagacy ofConstants.h"
cat ofConstants.h | grep OF_USE_LEGACY_VECTOR_MATH

# Compile openFrameworks
cd /home/pi/${OF_DIR}/libs/openFrameworksCompiled/project
make Release -j$(nproc)

# Compile portalApp
cd /home/pi/${OF_DIR}/apps/myApps/portalApp
make -j$(nproc)

# Configure systemd
cd systemd
sed -i "s+/home/pi/ofx+/home/pi/${OF_DIR}+g" portal-sensor.service
sed -i "s+/home/pi/ofx+/home/pi/${OF_DIR}+g" portal-display.service
mv portal-sensor.service /etc/systemd/system/
mv portal-display.service /etc/systemd/system/
systemctl enable portal-sensor
systemctl enable portal-display

# TODO: Figure how to automatically select headphone output

EOF
