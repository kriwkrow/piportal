#include "ofApp.h"

ofApp::ofApp(){
	loadConfig();

	ofAddListener(mqtt.onOnline, this, &ofApp::onOnline);
	ofAddListener(mqtt.onOffline, this, &ofApp::onOffline);
	ofAddListener(mqtt.onMessage, this, &ofApp::onMessage);

	mqtt.begin(config.mqtt.host, config.mqtt.port);
	if(!mqtt.connect("kris", "", "")){
		cout << "Failed to connect to MQTT host "
			<< config.mqtt.host
			<< ":" << config.mqtt.port
			<< endl;
		std::exit(EXIT_FAILURE);
	}

	ofLog() << config.mqtt.host;
	ofLog() << config.mqtt.topic;
	ofLog() << config.mqtt.port;

	radius = 0.0f;
	portalIdleVolume = 0.0f;
	pi = (float)PI;
	halfPi = (float)PI / 2.0f;
	scanLineAngle = -halfPi;
	scanLineSpeed = (ofRandomuf() / 2.0f) + 0.1f;
	collapse = true;
}


void ofApp::setup(){
	ofBackground(0);
	ofSetFrameRate(30);

	radius = 0.4f;

    etherdream.setup();
    etherdream.setPPS(35000);

	ildaFrame.params.output.transform.doFlipX = true;
    ildaFrame.params.output.transform.doFlipY = true;

	portalIdle.load("168910__couchhero__portal-idle.wav");
	portalIdle.setLoop(true);
	portalIdle.play();
	portalIdle.setVolume(portalIdleVolume);

	cout << "ofRandomuf()/2 " << ofRandomuf()/2 << endl;
}

void ofApp::update(){
	mqtt.update();

	distance.actual = ofLerp(distance.actual, distance.target, config.sensor.lerpingAmount);
	//ofLog() << "distance: " << distance.actual;

	if (distance.actual > config.sensor.distance.min &&
	    distance.actual < config.sensor.distance.max) {
		collapse = false;
	}else{
		collapse = true;
	}

	if(collapse){
		radius = radius *= 0.9f;
		portalIdleVolume = portalIdleVolume *= 0.9f;
	}else{
		float d = 0.4f - radius;
		d *= 0.9f;
		radius = 0.4f - d;

		float s = 1.0f - portalIdleVolume;
		s *= 0.9f;
		portalIdleVolume = 1.0f - s;
	}
	portalIdle.setVolume(portalIdleVolume);

	ildaFrame.clear();
	ofPolyline p;
	for(float angle = -pi; angle < pi; angle += 0.01f){
		float x = cos(angle) * radius + 0.5f;
		float y = sin(angle) * radius + 0.5f;
		p.addVertex(ofPoint(x, y));
	}
	p.setClosed(true);
	ildaFrame.addPoly(p);

	float halfPi = (float)PI / 2.0f;
	float scanLineIncr = ofGetLastFrameTime() * scanLineSpeed;

	if(!collapse){
		ofPolyline sl;
		float x = cos(scanLineAngle) * radius + 0.5f;
		float y = sin(scanLineAngle) * radius + 0.5f;
		sl.addVertex(ofPoint(x, y));

		x = cos(-scanLineAngle - PI) * radius + 0.5f;
		y = sin(-scanLineAngle - PI) * radius + 0.5f;
		sl.addVertex(ofPoint(x, y));

		ildaFrame.addPoly(sl);

		scanLineAngle += scanLineIncr;
		if(scanLineAngle > halfPi){
			scanLineAngle = -halfPi;
			scanLineSpeed = (ofRandomuf() / 2.0f) + 0.1f;
		}
	}

	ildaFrame.update();
	ildaFrame.draw(0, 0, ofGetWidth(), ofGetHeight());
	etherdream.setPoints(ildaFrame);
}

void ofApp::draw(){
	//
}

void ofApp::loadConfig(){
	ofFile file;
	file.open("config.xml");
	ofBuffer buffer = file.readToBuffer();

	ofXml xml;
	xml.load( buffer );

	//xml.set("config");

	#ifdef TARGET_RASPBERRY_PI
		config.mqtt.host = "127.0.0.1";
	#else
		config.mqtt.host = xml.findFirst("config/mqtt/host").getValue();
	#endif
	config.mqtt.topic = xml.findFirst("config/mqtt/topic").getValue();
	config.mqtt.port = xml.findFirst("config/mqtt/port").getIntValue();

	config.sensor.lerpingAmount = xml.findFirst("config/sensor/lerpingAmount").getFloatValue();
	config.sensor.distance.min = xml.findFirst("config/sensor/distance/min").getFloatValue();
	config.sensor.distance.max = xml.findFirst("config/sensor/distance/max").getFloatValue();

	cout << "Sensor lerpingAmount: " << config.sensor.lerpingAmount << endl;
	cout << "Sensor min distance: " << config.sensor.distance.min << endl;
	cout << "Sensor max distance: " << config.sensor.distance.max << endl;
}

void ofApp::keyPressed(int key){
	if(key == ' '){
		collapse = !collapse;
		scanLineAngle = -halfPi;
	}
}

void ofApp::onOnline(){
	cout << "MQTT online" << endl;
	mqtt.subscribe(config.mqtt.topic);
}

void ofApp::onOffline(){
	cout << "MQTT offline" << endl;
}

void ofApp::onMessage(ofxMQTTMessage & msg){
	cout << "MQTT message: " << msg.topic << ": " << msg.payload << endl;

	float newDistance = ofToFloat(msg.payload);

	/*
	if(newDistance < config.sensor.distance.min){
		newDistance = config.sensor.distance.min;
	}else if(newDistance > config.sensor.distance.max){
		newDistance = config.sensor.distance.max;
	}
	*/

	distance.target = newDistance;
}
