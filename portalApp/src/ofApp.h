#pragma once

#include "ofMain.h"
#include "ofxEtherdream.h"
#include "ofxMQTT.h"

struct ConfigParams{
	struct MqttParams{
		string host;
		string topic;
		int port;
	};
	MqttParams mqtt;

	struct SensorParams{
		float lerpingAmount;

		struct DistanceParams{
			float min;
			float max;
		};
		DistanceParams distance;
	};
	SensorParams sensor;
};

struct DistanceType{
	float actual;
	float target;
};

class ofApp : public ofBaseApp{
	public:
		ofApp();

		void setup();
		void update();
		void draw();

		void loadConfig();
		void keyPressed(int key);

		void onOnline();
		void onOffline();
		void onMessage(ofxMQTTMessage & msg);

		ofxIlda::Frame ildaFrame;
    ofxEtherdream etherdream;

		ofSoundPlayer portalIdle;

		float portalIdleVolume;
		float radius;
		float pi;
		float halfPi;
		float scanLineAngle; // from 0 to PI
		float scanLineSpeed;

		bool collapse;

		ConfigParams config;

		ofxMQTT mqtt;
		DistanceType distance;
};
