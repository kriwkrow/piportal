import RPi.GPIO as GPIO
import time
import paho.mqtt.client as mqtt

'''' 147e-6 from the MB1000. http://maxbotix.com/documents/LV-MaxSonar-EZ_Datasheet.pdf
It defaults to inches. '''
INCH_TO_CM = 2.54
SECONDS_TO_CM = 1/147e-6 * INCH_TO_CM

'''' It is the BCM27 from here https://pinout.xyz/pinout/pin16_gpio27# '''
SENSOR_PIN = 27

'''Sensor measures every 49ms. That is probably to fast from what we need.
This defines the number of measurments to be averaged. 
An example. 3 means that it will send averaged data every 3x49ms ''' 
SAMPLES_PER_AVERAGING = 1 

'''The reading of the sensor fluctuate a lot. This is the minimal resolution
in centimeters. Measured values will be multiples of this value'''
ROUND_TO = 1 

MQTT_BROKER_IP = "127.0.0.1"
MQTT_TOPIC = "us-distance-1/"

mqttc =  mqtt.Client("python_pub")
mqttc.connect(MQTT_BROKER_IP, port=1883, keepalive=60, bind_address="")
mqttc.loop()

GPIO.setmode(GPIO.BCM)
GPIO.setup(SENSOR_PIN, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

average_samples = []
while True:
    GPIO.wait_for_edge(SENSOR_PIN, GPIO.RISING)
    start_high = time.time()
    GPIO.wait_for_edge(SENSOR_PIN, GPIO.FALLING)
    end_high = time.time()
    high_width = end_high - start_high
    centimeters = high_width * SECONDS_TO_CM
    average_samples.append(centimeters)
    if len(average_samples) == SAMPLES_PER_AVERAGING:
        averaged_distance = sum(average_samples)/len(average_samples)
        averaged_distance_rounded = int(averaged_distance // ROUND_TO * ROUND_TO)
        mqttc.publish(MQTT_TOPIC, averaged_distance_rounded)
        average_samples.clear()
        print(averaged_distance_rounded)
