## Description

This service is responsible for reading the data from the MB1000 utrasonic sensor and publishing it to an qtt channel.


## Physical connection

[RPi pinnout](https://pinout.xyz/pinout/pin16_gpio23)

[Sensor manual](http://www.maxbotix.com/documents/LV-MaxSonar-EZ_Datasheet.pdf)

Supply the 3.3v and ground to the sensor from the RPi.

Connect the Pin-2-PW from the sensor to PIN 23 of the RPi.




## Dependencies on the RPi python 3.6 on the RPi

    sudo apt-get install build-essential libncursesw5-dev libgdbm-dev libc6-dev zlib1g-dev libsqlite3-dev tk-dev libssl-dev openssl
    wget https://www.python.org/ftp/python/3.6.0/Python-3.6.0.tgz
    tar -zxvf Python-3.6.0.tgz
    cd Python-3.6.0
    ./configure
    make
    sudo make install
    sudo pip3 install RPi.GPIO
    sudo pip3 install paho-mqtt
    git clone https://github.com/kr15h/the-thirsty-trees.git
    cd the-thirsty-trees
    git checkout data-aquisitor
    cd data_aquisitor
    python3 get_distance.py
    

## On the host

The host part is for running the mqtt broker. And for testing if the messages are being recieved on the device.

    sudo apt-get install mosquitto
