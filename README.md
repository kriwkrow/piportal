# PiPortal

A disk image that turns your Raspberry Pi into portal.

## Requirements

PiPortal has been tested on Raspberry Pi 3. You will need a ILDA compatible laser display and Etherdream DAC. You will also need a [MaxBotix Inc](https://www.maxbotix.com/product-category/lv-maxsonar-ez-products) range finder device that should be connected to the Raspberry Pi.

## Usage

Download the disk image and use Raspberry Pi Imager to install it on your SD card. Make sure all the peripherals are connected to your Raspberry Pi and power it up!
